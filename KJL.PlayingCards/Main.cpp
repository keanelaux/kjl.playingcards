
// Playing Cards
// Keane Laux

#include <iostream>
#include <conio.h>
using namespace std;

enum Rank{ TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE };

enum Suit{ SPADES, DIAMONDS, CLUBS, HEARTS };

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{

	(void)_getch();
	return 0;
}
